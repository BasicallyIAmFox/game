package net.mineclick.game.service;

import net.mineclick.game.Game;
import net.mineclick.game.model.GamePlayer;
import net.mineclick.global.service.ConfigurationsService;
import net.mineclick.global.util.SingletonInit;

import java.util.ArrayList;
import java.util.List;

@SingletonInit
public class LevelsService {
    private static LevelsService i;

    private List<Long> expList = new ArrayList<>();

    private LevelsService() {
        ConfigurationsService.i().onUpdate("levels", this::update);
    }

    public static LevelsService i() {
        return i == null ? i = new LevelsService() : i;
    }

    private void update() {
        if (!ConfigurationsService.i().contains("levels")) return;

        List<Long> levels = ConfigurationsService.i().get().getLongList("levels");
        if (levels.isEmpty()) {
            Game.i().getLogger().warning("Could not load Levels");
            return;
        }

        expList = levels;
        Game.i().getLogger().info("Loaded " + expList.size() + " EXP Levels");
    }

    /**
     * Get the level for a given exp amount
     *
     * @param exp The exp amount
     * @return The level for this exp amount
     */
    public int getLevel(long exp) {
        for (int i = expList.size() - 1; i >= 0; i--) {
            if (exp >= expList.get(i)) {
                return i + 1;
            }
        }

        return 1;
    }

    /**
     * Get exp needed for a given level
     *
     * @param level The level
     * @return The exp amount
     */
    public long getLevelExp(int level) {
        if (expList.isEmpty() || level <= 0) return 0;

        return level >= expList.size()
                ? expList.get(expList.size() - 1)
                : expList.get(level - 1);
    }

    /**
     * Get the delta needed to achieve the next level
     *
     * @param level The current level
     * @return The exp delta
     */
    public long getLevelDelta(int level) {
        return getLevelExp(level + 1) - getLevelExp(level);
    }

    public void updateExpBar(GamePlayer player) {
        if (player.isOffline())
            return;

        int level = getLevel(player.getExp());
        float percent = (player.getExp() - getLevelExp(level)) / (float) getLevelDelta(level);
        player.getPlayer().setExp(percent);
        player.getPlayer().setLevel(level);
    }
}
