package net.mineclick.game.model;

import lombok.Data;

@Data
public class PlayerPendingData {
    int votes;
}
