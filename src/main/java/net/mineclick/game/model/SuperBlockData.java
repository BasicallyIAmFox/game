package net.mineclick.game.model;

import lombok.Data;

@Data
public class SuperBlockData {
    private double chance = 0;
    private int clicksInARow = 0;
}
